package com.ey.hk.tigerclub.authentication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.ey.hk.tigerclub.R
import kotlinx.android.synthetic.main.activity_authentication.*


interface AuthenticationActivityDisplayLogic {
    fun displayAuthenticationData(viewModel: AuthenticationViewModel)
}


class AuthenticationActivity : AppCompatActivity(), AuthenticationActivityDisplayLogic {
    var interactor: AuthenticationInteractorBusinessLogic? = null
    var router: AuthenticationRouter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //do the setup
        setContentView(R.layout.activity_authentication)

        AuthenticationConfigurator.INSTANCE.configure(this)
        val aAuthenticationRequest = AuthenticationRequest()
        //populate the request


        interactor?.fetchAuthenticationData(aAuthenticationRequest)
        // Do other work

        init()
    }


    override fun displayAuthenticationData(viewModel: AuthenticationViewModel) {
        Log.e(TAG, "displayAuthenticationData() called with: viewModel = [$viewModel]")
        // Deal with the data
    }

    companion object {

        var TAG = AuthenticationActivity::class.java.simpleName
    }

    private fun init(){

        editTextEmail.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        editTextPassword.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

        submitButton.setOnClickListener{
            interactor?.authenticate()
        }
    }
}
