package com.ey.hk.tigerclub.authentication

import java.lang.ref.WeakReference


enum class AuthenticationConfigurator {
    INSTANCE;

    fun configure(activity: AuthenticationActivity) {

        val router = AuthenticationRouter()
        router.activity = WeakReference(activity)

        val presenter = AuthenticationPresenter()
        presenter.activity = activity

        val interactor = AuthenticationInteractor()
        interactor.presenter = presenter

        if (activity.interactor == null) {
            activity.interactor = interactor
        }
        if (activity.router == null) {
            activity.router = router
        }
    }
}
