package com.ey.hk.tigerclub.authentication

import android.util.Log

interface AuthenticationInteractorBusinessLogic {
    fun fetchAuthenticationData(request: AuthenticationRequest)
    fun authenticate()
}


class AuthenticationInteractor : AuthenticationInteractorBusinessLogic {
    var presenter: AuthenticationPresenterPresentationLogic? = null

    override fun fetchAuthenticationData(request: AuthenticationRequest) {
        val AuthenticationResponse = AuthenticationResponse()
        // Call the workers

        presenter?.presentAuthenticationData(AuthenticationResponse)
    }

    override fun authenticate(){

    }

    companion object {

        var TAG = AuthenticationInteractor::class.java.simpleName
    }
}
