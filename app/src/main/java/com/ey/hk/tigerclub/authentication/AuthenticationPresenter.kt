package com.ey.hk.tigerclub.authentication

import android.util.Log

import java.lang.ref.WeakReference
import java.util.ArrayList
import java.util.Calendar
import java.util.concurrent.TimeUnit

interface AuthenticationPresenterPresentationLogic {
    fun presentAuthenticationData(response: AuthenticationResponse)
}


class AuthenticationPresenter : AuthenticationPresenterPresentationLogic {

    //weak var output: HomePresenterOutput!
    var activity: AuthenticationActivityDisplayLogic? = null


    override fun presentAuthenticationData(response: AuthenticationResponse) {
        // Log.e(TAG, "presentAuthenticationData() called with: response = [" + response + "]");
        //Do your decoration or filtering here

    }

    companion object {

        var TAG = AuthenticationPresenter::class.java.simpleName
    }


}
