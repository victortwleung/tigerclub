package com.ey.hk.tigerclub.landing

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.ey.hk.tigerclub.R
import com.ey.hk.tigerclub.authentication.AuthenticationActivity
import kotlinx.android.synthetic.main.activity_landing.*
import org.jetbrains.anko.intentFor


interface LandingActivityDisplayLogic {
    fun displayLandingData(viewModel: LandingViewModel)
}


class LandingActivity : AppCompatActivity(), LandingActivityDisplayLogic {
    var interactor: LandingInteractorBusinessLogic? = null
    var router: LandingRouter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //do the setup
        setContentView(R.layout.activity_landing)

        LandingConfigurator.INSTANCE.configure(this)
        val aLandingRequest = LandingRequest()
        //populate the request


        interactor?.fetchLandingData(aLandingRequest)
        // Do other work

        init()
    }


    override fun displayLandingData(viewModel: LandingViewModel) {
        Log.e(TAG, "displayLandingData() called with: viewModel = [$viewModel]")
        // Deal with the data
    }

    companion object {

        var TAG = LandingActivity::class.java.simpleName
    }

    private fun init(){

        loginButton.setOnClickListener {
            router?.routeToSignIn()
        }

        signUpButton.setOnClickListener {
            router?.routeToSignUp()
        }
    }
}
