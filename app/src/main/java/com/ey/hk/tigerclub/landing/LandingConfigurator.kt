package com.ey.hk.tigerclub.landing

import java.lang.ref.WeakReference


enum class LandingConfigurator {
    INSTANCE;

    fun configure(activity: LandingActivity) {

        val router = LandingRouter()
        router.activity = activity

        val presenter = LandingPresenter()
        presenter.activity = activity

        val interactor = LandingInteractor()
        interactor.presenter = presenter

        if (activity.interactor == null) {
            activity.interactor = interactor
        }
        if (activity.router == null) {
            activity.router = router
        }
    }
}
