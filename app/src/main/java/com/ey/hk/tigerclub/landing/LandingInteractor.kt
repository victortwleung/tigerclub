package com.ey.hk.tigerclub.landing

import android.util.Log

interface LandingInteractorBusinessLogic {
    fun fetchLandingData(request: LandingRequest)
}


class LandingInteractor : LandingInteractorBusinessLogic {
    var presenter: LandingPresenterPresentationLogic? = null

    override fun fetchLandingData(request: LandingRequest) {
        val LandingResponse = LandingResponse()
        // Call the workers

        presenter?.presentLandingData(LandingResponse)
    }

    companion object {

        var TAG = LandingInteractor::class.java.simpleName
    }
}
