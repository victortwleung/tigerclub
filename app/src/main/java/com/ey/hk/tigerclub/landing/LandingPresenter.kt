package com.ey.hk.tigerclub.landing

import android.util.Log

import java.lang.ref.WeakReference
import java.util.ArrayList
import java.util.Calendar
import java.util.concurrent.TimeUnit

interface LandingPresenterPresentationLogic {
    fun presentLandingData(response: LandingResponse)
}


class LandingPresenter : LandingPresenterPresentationLogic {

    var activity: LandingActivityDisplayLogic? = null

    override fun presentLandingData(response: LandingResponse) {
        // Log.e(TAG, "presentLandingData() called with: response = [" + response + "]");
        //Do your decoration or filtering here

    }

    companion object {

        var TAG = LandingPresenter::class.java.simpleName
    }


}
