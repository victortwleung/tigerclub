package com.ey.hk.tigerclub.landing

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import com.ey.hk.tigerclub.authentication.AuthenticationActivity
import org.jetbrains.anko.intentFor

import java.lang.ref.WeakReference


interface LandingRouterInput {
    fun routeToSignUp()
    fun routeToSignIn()
}

class LandingRouter : LandingRouterInput{
    var activity: LandingActivity? = null

    override fun routeToSignUp(){
        val intent = activity?.intentFor<AuthenticationActivity>()
        intent?.let {
            activity?.startActivity(it)
        }
    }

    override fun routeToSignIn(){
        val intent = activity?.intentFor<AuthenticationActivity>()
        intent?.let {
            activity?.startActivity(it)
        }
    }

    companion object {

        var TAG = LandingRouter::class.java.simpleName
    }


}
